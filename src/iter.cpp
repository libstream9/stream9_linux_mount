#include <stream9/libmount/iter.hpp>

#include <stream9/libmount/error.hpp>

#include <cassert>
#include <utility>

namespace stream9::libmount {

iter::
iter(int const direction)
    : m_handle { ::mnt_new_iter(direction) }
{
    if (!m_handle) {
        throw error { "mnt_new_iter()", make_error_code(errno) };
    }
}

iter::
~iter() noexcept
{
    if (m_handle) {
        ::mnt_free_iter(m_handle);
    }
}

iter::
iter(iter&& other) noexcept
    : m_handle { other.m_handle }
{
    m_handle = nullptr;
}

iter& iter::
operator=(iter&& other) noexcept
{
    swap(*this, other);
    return *this;
}

void
swap(iter& lhs, iter& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_handle, rhs.m_handle);
}

int iter::
direction() const noexcept
{
    assert(m_handle);

    return ::mnt_iter_get_direction(m_handle);
}

void iter::
reset(int const direction) noexcept
{
    assert(m_handle);

    ::mnt_reset_iter(m_handle, direction);
}

} // namespace stream9::libmount
