#include <stream9/libmount/iterator.hpp>

#include <stream9/libmount/error.hpp>

namespace stream9::libmount {

/*
 * table_iterator
 */
table_iterator::
table_iterator(table const& t, int const direction)
    : m_table { t }
{
    m_it = std::shared_ptr<::libmnt_iter> {
        ::mnt_new_iter(direction),
        ::mnt_free_iter
    };
    if (!m_it) {
        throw error { "mnt_new_iter()", make_error_code(errno) };
    }

    increment();
}

int table_iterator::
direction() const noexcept
{
    return ::mnt_iter_get_direction(m_it.get());
}

void table_iterator::
set(fs const& f)
{
    auto const rc = ::mnt_table_set_iter(m_table, m_it.get(), f);
    if (rc < 0) {
        throw error { "mnt_table_set_iter()", make_error_code(-rc) };
    }
}

void table_iterator::
reset() noexcept
{
    ::mnt_reset_iter(m_it.get(), -1);
}

void table_iterator::
increment()
{
    ::libmnt_fs* f {};
    auto const rc = ::mnt_table_next_fs(m_table, m_it.get(), &f);
    if (rc == 0) {
        m_val = fs { *f };
    }
    else if (rc == 1) {
        m_end = true;
    }
    else {
        assert(rc < 0);
        throw error { "mnt_table_next_fs()", make_error_code(-rc) };
    }
}

/*
 * child_iterator
 */
child_iterator::
child_iterator(table const& t, fs const& parent,
               int const direction)
    : m_table { t }
    , m_parent { parent }
{
    m_it = std::shared_ptr<::libmnt_iter> {
        ::mnt_new_iter(direction),
        ::mnt_free_iter
    };
    if (!m_it) {
        throw error { "mnt_new_iter()", make_error_code(errno) };
    }

    increment();
}

int child_iterator::
direction() const noexcept
{
    return ::mnt_iter_get_direction(m_it.get());
}

void child_iterator::
set(fs const& f)
{
    auto const rc = ::mnt_table_set_iter(m_table, m_it.get(), f);
    if (rc < 0) {
        throw error { "mnt_table_set_iter()", make_error_code(-rc) };
    }
}

void child_iterator::
reset() noexcept
{
    ::mnt_reset_iter(m_it.get(), -1);
}

void child_iterator::
increment()
{
    ::libmnt_fs* f {};
    auto const rc =
        ::mnt_table_next_child_fs(m_table, m_it.get(), m_parent, &f);
    if (rc == 0) {
        m_val = fs { *f };
    }
    else if (rc == 1) {
        m_end = true;
    }
    else {
        assert(rc < 0);
        throw error { "mnt_table_next_child_fs()", make_error_code(-rc) };
    }
}

} // namespace stream9::libmount
