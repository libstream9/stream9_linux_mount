#include <stream9/libmount/update.hpp>

#include <stream9/libmount/error.hpp>
#include <stream9/libmount/fs.hpp>
#include <stream9/libmount/lock.hpp>

#include <utility>

#include <stream9/json.hpp>

namespace stream9::libmount {

update::
update()
    : m_handle { ::mnt_new_update() }
{
    if (!m_handle) {
        throw error { "mnt_new_update()", make_error_code(errno) };
    }
}

update::
~update() noexcept
{
    if (m_handle) {
        ::mnt_free_update(m_handle);
    }
}

update::
update(update&& other) noexcept
    : m_handle { other.m_handle }
{
    other.m_handle = nullptr;
}

update& update::
operator=(update&& other) noexcept
{
    swap(*this, other);

    return *this;
}

void
swap(update& lhs, update& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_handle, rhs.m_handle);
}

bool update::
is_ready() const noexcept
{
    assert(m_handle);

    return ::mnt_update_is_ready(m_handle);
}

opt<cstring_view> update::
filename() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_update_get_filename(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<class fs> update::
fs() const noexcept
{
    assert(m_handle);

    opt<class fs> result;

    if (auto* const ptr = ::mnt_update_get_fs(m_handle)) {
        result.emplace(*ptr);
    }

    return result;
}

unsigned long update::
mflags() const noexcept
{
    assert(m_handle);

    return ::mnt_update_get_mflags(m_handle);
}

void update::
set_mount_fs(unsigned long const mountflags, class fs const& fs)
{
    assert(m_handle);

    auto const rc = ::mnt_update_set_fs(m_handle, mountflags, nullptr, fs);
    if (rc < 0) {
        throw error {
            "mnt_update_set_fs()",
            make_error_code(-rc), {
                { "mountflags", mountflags },
                { "fs", fs },
            }
        };
    }
}

void update::
set_umount_target(unsigned long const mountflags, cstring_view const target)
{
    assert(m_handle);

    auto const rc = ::mnt_update_set_fs(m_handle, mountflags, target, nullptr);
    if (rc < 0) {
        throw error {
            "mnt_update_set_fs()",
            make_error_code(-rc), {
                { "mountflags", mountflags },
                { "target", target },
            }
        };
    }
}

void update::
update_table(opt<lock> lk/*= {}*/)
{
    assert(m_handle);

    auto* const l = lk ? static_cast<libmnt_lock*>(*lk) : nullptr;

    auto const rc = ::mnt_update_table(m_handle, l);
    if (rc < 0) {
        throw error { "mnt_update_table()", make_error_code(-rc) };
    }
}

void update::
force_rdonly(bool const rdonly)
{
    assert(m_handle);

    auto const rc = ::mnt_update_force_rdonly(m_handle, rdonly);
    if (rc < 0) {
        throw error {
            "mnt_update_force_rdonly()",
            make_error_code(-rc), {
                { "rdonly", rdonly },
            }
        };
    }
}

} // namespace stream9::libmount
