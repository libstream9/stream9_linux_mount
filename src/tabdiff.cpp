#include <stream9/libmount/tabdiff.hpp>

#include <ostream>

#include <stream9/libmount/error.hpp>

#include <stream9/json.hpp>

namespace stream9::libmount {

tabdiff::
tabdiff(table const& old_tab, table const& new_tab)
    : m_handle { ::mnt_new_tabdiff() }
{
    if (!m_handle) {
        throw error { "mnt_new_tabdiff()", make_error_code(errno) };
    }

    auto const rc = ::mnt_diff_tables(m_handle, old_tab, new_tab);
    if (rc < 0) {
        throw error { "mnt_diff_tables()", make_error_code(-rc) };
    }
}

tabdiff::
~tabdiff() noexcept
{
    if (m_handle) {
        ::mnt_free_tabdiff(m_handle);
    }
}

tabdiff::
tabdiff(tabdiff&& other) noexcept
    : m_handle { other.m_handle }
{
    other.m_handle = nullptr;
}

tabdiff& tabdiff::
operator=(tabdiff&& other) noexcept
{
    swap(*this, other);

    return *this;
}

void
swap(tabdiff& lhs, tabdiff& rhs) noexcept
{
    using std::swap;

    swap(lhs.m_handle, rhs.m_handle);
}

diff_iterator tabdiff::
begin() const
{
    return { *this, MNT_ITER_FORWARD };
}

std::default_sentinel_t tabdiff::
end() const noexcept
{
    return {};
}

diff_iterator tabdiff::
rbegin() const
{
    return { *this, MNT_ITER_BACKWARD };
}

std::default_sentinel_t tabdiff::
rend() const noexcept
{
    return {};
}

/*
 * diff_iterator
 */
diff_iterator::
diff_iterator(tabdiff const& d, int const direction)
    : m_diff { d }
{
    m_it = std::shared_ptr<::libmnt_iter> {
        ::mnt_new_iter(direction),
        ::mnt_free_iter
    };
    if (!m_it) {
        throw error { "mnt_new_iter()", make_error_code(errno) };
    }

    increment();
}

void diff_iterator::
increment()
{
    assert(m_diff);
    assert(m_it);
    assert(!m_end);

    ::libmnt_fs* ofs {};
    ::libmnt_fs* nfs {};
    int op {};

    auto const rc =
        ::mnt_tabdiff_next_change(m_diff, m_it.get(), &ofs, &nfs, &op);
    if (rc < 0) {
        throw error { "mnt_tabdiff_next_change()", make_error_code(-rc) };
    }
    if (rc == 1) {
        m_end = true;
    }
    else {
        if (ofs) {
            m_val.old_fs.emplace(*ofs);
        }
        else {
            m_val.old_fs.reset();
        }
        if (nfs) {
            m_val.new_fs.emplace(*nfs);
        }
        else {
            m_val.new_fs.reset();
        }
        m_val.oper = op;
    }
}

static auto
oper_to_symbol(int const oper)
{
    switch (oper) {
        case MNT_TABDIFF_MOUNT:
            return "MNT_TABDIFF_MOUNT";
        case MNT_TABDIFF_UMOUNT:
            return "MNT_TABDIFF_UMOUNT";
        case MNT_TABDIFF_MOVE:
            return "MNT_TABDIFF_MOVE";
        case MNT_TABDIFF_REMOUNT:
            return "MNT_TABDIFF_REMOUNT";
        case MNT_TABDIFF_PROPAGATION:
            return "MNT_TABDIFF_PROPAGATION";
    }

    return "unknown value";
}

void
tag_invoke(json::value_from_tag, json::value& v, diff_result const& d)
{
    auto& obj = v.emplace_object();

    if (d.old_fs) {
        obj["old_fs"] = json::value_from(*d.old_fs);
    }
    if (d.new_fs) {
        obj["new_fs"] = json::value_from(*d.new_fs);
    }

    obj["oper"] = oper_to_symbol(d.oper);
}

std::ostream&
operator<<(std::ostream& os, diff_result const& r)
{
    os << json::value_from(r);
    return os;
}

} // namespace stream9::libmount
