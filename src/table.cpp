#include <stream9/libmount/table.hpp>

#include "namespace.hpp"

#include <stream9/libmount/cache.hpp>
#include <stream9/libmount/child_range.hpp>
#include <stream9/libmount/error.hpp>
#include <stream9/libmount/fs.hpp>
#include <stream9/libmount/iterator.hpp>
#include <stream9/libmount/namespace.hpp>

#include <cassert>
#include <utility>

#include <stream9/json.hpp>

namespace stream9::libmount {

table::
table()
    : m_handle { ::mnt_new_table() }
{
    if (!m_handle) {
        throw error { "mnt_new_table()", make_error_code(errno) };
    }
}

table::
table(::libmnt_table& h) noexcept
    : m_handle { &h }
{}

table table::
from_file(cstring_view const filename)
{
    auto const h = ::mnt_new_table_from_file(filename);
    if (!h) {
        throw error { "mnt_new_table_from_file", make_error_code(errno) };
    }

    return table { *h };
}

table table::
from_dir(cstring_view const dirname)
{
    auto const h = ::mnt_new_table_from_dir(dirname);
    if (!h) {
        throw error { "mnt_new_table_from_dir", make_error_code(errno) };
    }

    return table { *h };
}

table::
~table() noexcept
{
    if (m_handle) {
        ::mnt_unref_table(m_handle);
    }
}

table::
table(table const& other) noexcept
    : m_handle { other.m_handle }
{
    assert(other.m_handle);

    ::mnt_ref_table(m_handle);
}

table& table::
operator=(table const& other) noexcept
{
    assert(other.m_handle);

    table tmp { other };
    swap(*this, tmp);

    return *this;
}

table::
table(table&& other) noexcept
    : m_handle { other.m_handle }
{
    other.m_handle = nullptr;
}

table& table::
operator=(table&& other) noexcept
{
    assert(other.m_handle);
    swap(*this, other);

    return *this;
}

void
swap(table& lhs, table& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_handle, rhs.m_handle);
}

table_iterator table::
begin() const
{
    return { *this, MNT_ITER_FORWARD };
}

std::default_sentinel_t table::
end() const noexcept
{
    return {};
}

table_iterator table::
rbegin() const
{
    return { *this, MNT_ITER_BACKWARD };
}

std::default_sentinel_t table::
rend() const noexcept
{
    return {};
}

int table::
size() const noexcept
{
    assert(m_handle);

    return ::mnt_table_get_nents(m_handle);
}

bool table::
empty() const noexcept
{
    assert(m_handle);

    return ::mnt_table_is_empty(m_handle);
}

void* table::
userdata() const noexcept
{
    assert(m_handle);

    return ::mnt_table_get_userdata(m_handle);
}

bool table::
with_comments() const noexcept
{
    assert(m_handle);

    return ::mnt_table_with_comments(m_handle);
}

opt<cstring_view> table::
intro_comment() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const ptr = ::mnt_table_get_intro_comment(m_handle);
    if (ptr) {
        result.emplace(ptr);
    }

    return result;
}

opt<cstring_view> table::
trailing_comment() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const ptr = ::mnt_table_get_trailing_comment(m_handle);
    if (ptr) {
        result.emplace(ptr);
    }

    return result;
}

opt<class cache> table::
cache() const noexcept
{
    assert(m_handle);
    opt<class cache> result;

    auto* const ptr = ::mnt_table_get_cache(m_handle);
    if (ptr != nullptr) {
        ::mnt_ref_cache(ptr);
        result.emplace(*ptr);
    }

    return result;
}

int table::
find_fs(fs const& f) const noexcept
{
    assert(m_handle);

    return ::mnt_table_find_fs(m_handle, f);
}

fs table::
first_fs() const noexcept
{
    assert(m_handle);
    assert(!empty());

    ::libmnt_fs* ptr {};
    ::mnt_table_first_fs(m_handle, &ptr);

    assert(ptr);
    return fs { *ptr };
}

fs table::
last_fs() const noexcept
{
    assert(m_handle);
    assert(!empty());

    ::libmnt_fs* ptr {};
    ::mnt_table_last_fs(m_handle, &ptr);

    assert(ptr);
    return fs { *ptr };
}

opt<fs> table::
over_fs(fs const& parent) const
{
    assert(m_handle);

    opt<fs> result;
    ::libmnt_fs* child {};

    auto const rc = ::mnt_table_over_fs(m_handle, parent, &child);
    if (rc < 0) {
        throw error { "mnt_table_over_fs()", make_error_code(-rc), {
                { "parent", parent }
            }
        };
    }

    if (child) {
        result.emplace(*child);
    }

    return result;
}

fs table::
root_fs() const
{
    assert(m_handle);

    ::libmnt_fs* f {};

    auto const rc = ::mnt_table_get_root_fs(m_handle, &f);
    if (rc < 0) {
        throw error { "mnt_table_get_root_fs()", make_error_code(-rc) };
    }

    assert(f);
    return fs { *f };
}

opt<fs> table::
find_mountpoint(cstring_view const path, int const direction) const noexcept
{
    assert(m_handle);

    opt<fs> result;

    auto* const fs = ::mnt_table_find_mountpoint(m_handle, path, direction);
    if (fs) {
        result.emplace(*fs);
    }

    return result;
}

opt<fs> table::
find_target(cstring_view const path, int const direction) const noexcept
{
    assert(m_handle);

    opt<fs> result;

    auto* const fs = ::mnt_table_find_target(m_handle, path, direction);
    if (fs) {
        result.emplace(*fs);
    }

    return result;
}

opt<fs> table::
find_srcpath(cstring_view const path, int const direction) const noexcept
{
    assert(m_handle);

    opt<fs> result;

    auto* const fs = ::mnt_table_find_srcpath(m_handle, path, direction);
    if (fs) {
        result.emplace(*fs);
    }

    return result;
}

opt<fs> table::
find_tag(cstring_view const tag,
         cstring_view const val, int const direction) const noexcept
{
    assert(m_handle);

    opt<fs> result;

    auto* const fs = ::mnt_table_find_tag(m_handle, tag, val, direction);
    if (fs) {
        result.emplace(*fs);
    }

    return result;
}

opt<fs> table::
find_target_with_option(cstring_view const path,
                        cstring_view const option,
                        cstring_view const val,
                        int const direction) const noexcept
{
    assert(m_handle);

    opt<fs> result;

    auto* const fs = ::mnt_table_find_target_with_option(
                                        m_handle, path, option, val, direction);
    if (fs) {
        result.emplace(*fs);
    }

    return result;
}

opt<fs> table::
find_source(cstring_view const source, int const direction) const noexcept
{
    assert(m_handle);

    opt<fs> result;

    auto* const fs = ::mnt_table_find_source(m_handle, source, direction);
    if (fs) {
        result.emplace(*fs);
    }

    return result;
}

opt<fs> table::
find_pair(cstring_view const source,
          cstring_view const target,
          int const direction) const noexcept
{
    assert(m_handle);

    opt<fs> result;

    auto* const fs = ::mnt_table_find_pair(m_handle, source, target, direction);
    if (fs) {
        result.emplace(*fs);
    }

    return result;
}

opt<fs> table::
find_devno(::dev_t const devno, int const direction) const noexcept
{
    assert(m_handle);

    opt<fs> result;

    auto* const fs = ::mnt_table_find_devno(m_handle, devno, direction);
    if (fs) {
        result.emplace(*fs);
    }

    return result;
}

bool table::
is_fs_mounted(fs const& f) const noexcept
{
    assert(m_handle);

    return ::mnt_table_is_fs_mounted(m_handle, f);
}

void table::
parse_stream(FILE* const f, cstring_view const filename)
{
    auto const rc = ::mnt_table_parse_stream(m_handle, f, filename);
    if (rc < 0) {
        throw error {
            "mnt_table_parse_stream",
            make_error_code(-rc), {
                { "filename", filename }
            }
        };
    }
}

void table::
parse_file(cstring_view const filename)
{
    auto const rc = ::mnt_table_parse_file(m_handle, filename);
    if (rc < 0) {
        throw error { "mnt_table_parse_file", make_error_code(-rc), {
                { "filename", filename }
            }
        };
    }
}

void table::
parse_dir(cstring_view const dirname)
{
    auto const rc = ::mnt_table_parse_dir(m_handle, dirname);
    if (rc < 0) {
        throw error { "mnt_table_parse_dir", make_error_code(-rc), {
                { "dirname", dirname }
            }
        };
    }
}

void table::
parse_fstab(opt<cstring_view> filename/*= {}*/)
{
    assert(m_handle);

    auto const rc = ::mnt_table_parse_fstab(
                              m_handle, filename ? filename->c_str() : nullptr);
    if (rc < 0) {
        json::object cxt;
        if (filename) {
            cxt["filename"] = *filename;
        }

        throw error {
            "mnt_table_parse_fstab()",
            make_error_code(-rc),
            std::move(cxt)
        };
    }
}

void table::
parse_swaps(opt<cstring_view> filename/*= {}*/)
{
    assert(m_handle);

    auto const rc = ::mnt_table_parse_swaps(
                              m_handle, filename ? filename->c_str() : nullptr);
    if (rc < 0) {
        json::object cxt;
        if (filename) {
            cxt["filename"] = *filename;
        }

        throw error {
            "mnt_table_parse_swaps()",
            make_error_code(-rc),
            std::move(cxt)
        };
    }
}

void table::
parse_mtab(opt<cstring_view> filename/*= {}*/)
{
    assert(m_handle);

    auto const rc = ::mnt_table_parse_mtab(
                              m_handle, filename ? filename->c_str() : nullptr);
    if (rc < 0) {
        json::object cxt;
        if (filename) {
            cxt["filename"] = *filename;
        }

        throw error {
            "mnt_table_parse_mtab()",
            make_error_code(-rc),
            std::move(cxt)
        };
    }
}

void table::
add_fs(fs const& f)
{
    assert(m_handle);

    auto const rc = ::mnt_table_add_fs(m_handle, f);
    if (rc < 0) {
        throw error {
            "mnt_table_add_fs()",
            make_error_code(-rc), {
                { "fs", f }
            }
        };
    }
}

void table::
insert_fs(bool const before, fs const* const pos, fs const& f)
{
    assert(m_handle);

    ::libmnt_fs* p {};
    if (pos == nullptr) {
        p = nullptr;
    } else {
        p = *pos;
    }

    auto const rc = ::mnt_table_insert_fs(m_handle, before, p, f);
    if (rc < 0) {
        throw error {
            "mnt_table_insert_fs()",
            make_error_code(-rc), {
                { "before", before },
                { "fs", f },
            }
        };
    }
}

void table::
remove_fs(fs const& f)
{
    assert(m_handle);

    auto const rc = ::mnt_table_remove_fs(m_handle, f);
    if (rc < 0) {
        throw error {
            "mnt_table_remove_fs()",
            make_error_code(-rc), {
                { "fs", f },
            }
        };
    }
}

void table::
uniq_fs(int const flags,
        int (*cmp)(struct libmnt_table*,
                   struct libmnt_fs*,
                   struct libmnt_fs* ))
{
    auto const rc = ::mnt_table_uniq_fs(m_handle, flags, cmp);
    if (rc < 0) {
        throw error {
            "mnt_table_uniq_fs()",
            make_error_code(-rc), {
                { "flags", flags }
            }
        };
    }
}

void table::
set_userdata(void* const data) noexcept
{
    assert(m_handle);

    ::mnt_table_set_userdata(m_handle, data);
}

void table::
enable_comments(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_table_enable_comments(m_handle, enable);
}

void table::
set_intro_comment(cstring_view const comm)
{
    assert(m_handle);

    auto const rc = ::mnt_table_set_intro_comment(m_handle, comm);
    if (rc < 0) {
        throw error {
            "mnt_table_set_intro_comment()",
            make_error_code(-rc), {
                { "comm", comm }
            }
        };
    }
}

void table::
append_intro_comment(cstring_view const comm)
{
    assert(m_handle);

    auto const rc = ::mnt_table_append_intro_comment(m_handle, comm);
    if (rc < 0) {
        throw error {
            "mnt_table_append_intro_comment()",
            make_error_code(-rc), {
                { "comm", comm }
            }
        };
    }
}

void table::
set_trailing_comment(cstring_view const comm)
{
    assert(m_handle);

    auto const rc = ::mnt_table_set_trailing_comment(m_handle, comm);
    if (rc < 0) {
        throw error {
            "mnt_table_set_trailing_comment()",
            make_error_code(-rc), {
                { "comm", comm }
            }
        };
    }
}

void table::
append_trailing_comment(cstring_view const comm)
{
    assert(m_handle);

    auto const rc = ::mnt_table_append_trailing_comment(m_handle, comm);
    if (rc < 0) {
        throw error {
            "mnt_table_append_trailing_comment()",
            make_error_code(-rc), {
                { "comm", comm }
            }
        };
    }
}

void table::
set_cache(class cache c) noexcept
{
    assert(m_handle);

    ::mnt_table_set_cache(m_handle, c);
}

void table::
set_parser_errcb(
    int (*cb)(struct ::libmnt_table*, char const* filename, int line) ) noexcept
{
    assert(m_handle);

    ::mnt_table_set_parser_errcb(m_handle, cb);
}

void table::
reset()
{
    assert(m_handle);

    auto const rc = ::mnt_reset_table(m_handle);
    if (rc < 0) {
        throw error { "mnt_reset_table()", make_error_code(-rc) };
    }
}

void
move_fs(table& src, table& dst, bool const before,
        fs const* const pos, fs const f)
{
    ::libmnt_fs* p {};
    if (pos == nullptr) {
        p = nullptr;
    }
    else {
        p = *pos;
    }

    auto const rc = ::mnt_table_move_fs(src, dst, before, p, f);
    if (rc < 0) {
        throw error {
            "mnt_table_move_fs()",
            make_error_code(-rc), {
                { "before", before },
                { "fs", f }
            }
        };
    }
}

void
write_file(table& tbl, FILE* const file)
{
    auto const rc = ::mnt_table_write_file(tbl, file);
    if (rc < 0) {
        throw error { "mnt_table_write_file()", make_error_code(-rc) };
    }
}

void
replace_file(table& tbl, cstring_view filename)
{
    auto const rc = ::mnt_table_replace_file(tbl, filename);
    if (rc < 0) {
        throw error {
            "mnt_table_replace_file()",
            make_error_code(-rc), {
                { "filename", filename },
            }
        };
    }
}

} // namespace stream9::libmount
