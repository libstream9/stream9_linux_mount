#include <stream9/libmount/optstr.hpp>

#include <stream9/libmount/error.hpp>

#include <cassert>
#include <utility>

#include <string.h>

#include <stream9/json.hpp>

namespace stream9::libmount {

/*
 * class optstr
 */
optstr::
optstr()
    : m_value { ::strdup("") }
{
    if (!m_value) {
        throw error { make_error_code(errno) };
    }
}

optstr::
optstr(std::string_view const s)
    : m_value { ::strndup(s.data(), s.size()) }
{
    if (!m_value) {
        throw error { make_error_code(errno), { { "s", s }, } };
    }
}

optstr::
~optstr() noexcept
{
    if (m_value) {
        ::free(m_value);
    }
}

optstr::
optstr(optstr const& other)
    : m_value { ::strdup(other.m_value) }
{
    assert(other.m_value);

    if (!m_value) {
        throw error { make_error_code(errno), { { "other", other.m_value }, } };
    }
}

optstr& optstr::
operator=(optstr const& other)
{
    optstr tmp { other };
    swap(*this, tmp);

    return *this;
}

optstr::
optstr(optstr&& other) noexcept
    : m_value { other.m_value }
{
    other.m_value = nullptr;
}

optstr& optstr::
operator=(optstr&& other) noexcept
{
    swap(*this, other);

    return *this;
}

void
swap(optstr& lhs, optstr& rhs) noexcept
{
    using std::swap;

    swap(lhs.m_value, rhs.m_value);
}

optstr::iterator optstr::
begin() const
{
    assert(m_value);

    return optstr_view(m_value).begin();
}

optstr::sentinel optstr::
end() const noexcept
{
    return {};
}

opt<std::string_view> optstr::
option(cstring_view const name) const
{
    assert(m_value);

    return optstr_view(m_value).option(name);
}

cstring optstr::
options(struct ::libmnt_optmap const* const map, int const ignore) const
{
    assert(m_value);

    return optstr_view(m_value).options(map, ignore);
}

unsigned long optstr::
flags(struct ::libmnt_optmap const* const map) const
{
    assert(m_value);

    return optstr_view(m_value).flags(map);
}

split_result optstr::
split(bool const ignore_user/*= false*/, bool const ignore_vfs/*= false*/) const
{
    assert(m_value);

    return optstr_view(m_value).split(ignore_user, ignore_vfs);
}

void optstr::
set_option(cstring_view const name, cstring_view const value)
{
    assert(m_value);

    auto const rc = ::mnt_optstr_set_option(&m_value, name, value);
    if (rc < 0) {
        throw error {
            "mnt_optstr_set_option()",
            make_error_code(-rc), {
                { "optstr", m_value },
                { "name", name },
                { "value", value },
            }
        };
    }
}

void optstr::
append_option(cstring_view const name, cstring_view const value)
{
    assert(m_value);

    auto const rc = ::mnt_optstr_append_option(&m_value, name, value);
    if (rc < 0) {
        throw error {
            "mnt_optstr_append_option()",
            make_error_code(-rc), {
                { "optstr", m_value },
                { "name", name },
                { "value", value },
            }
        };
    }
}

void optstr::
prepend_option(cstring_view const name, cstring_view const value)
{
    assert(m_value);

    auto const rc = ::mnt_optstr_prepend_option(&m_value, name, value);
    if (rc < 0) {
        throw error {
            "mnt_optstr_prepend_option()",
            make_error_code(-rc), {
                { "optstr", m_value },
                { "name", name },
                { "value", value },
            }
        };
    }
}

bool optstr::
remove_option(cstring_view const name)
{
    assert(m_value);

    auto const rc = ::mnt_optstr_remove_option(&m_value, name);
    if (rc == 0) {
        // nop
    }
    else if (rc == 1) {
        return false;
    }
    else if (rc < 0) {
        throw error {
            "mnt_optstr_remove_option()",
            make_error_code(-rc), {
                { "optstr", m_value },
                { "name", name },
            }
        };
    }
    else {
        assert(false);
    }

    return true;
}

bool optstr::
deduplicate_option(cstring_view const name)
{
    assert(m_value);

    auto const rc = ::mnt_optstr_deduplicate_option(&m_value, name);
    if (rc == 0) {
        // nop
    }
    else if (rc == 1) {
        return false;
    }
    else if (rc < 0) {
        throw error {
            "mnt_optstr_deduplicate_option()",
            make_error_code(-rc), {
                { "optstr", m_value },
                { "name", name },
            }
        };
    }
    else {
        assert(false);
    }

    return true;
}

void optstr::
apply_flags(unsigned long const flags,
            struct ::libmnt_optmap const* const map)
{
    assert(m_value);

    auto const rc = ::mnt_optstr_apply_flags(&m_value, flags, map);
    if (rc < 0) {
        throw error {
            "mnt_optstr_apply_flags()",
            make_error_code(-rc), {
                { "optstr", m_value },
                { "flags", flags },
            }
        };
    }
}

/*
 * class optstr_view
 */
optstr_view::
optstr_view(cstring_view const s) noexcept
    : m_value { s }
{}

optstr_view::iterator optstr_view::
begin() const
{
    assert(m_value);

    return const_cast<char*>(m_value);
}

optstr_view::sentinel optstr_view::
end() const noexcept
{
    return {};
}

opt<std::string_view> optstr_view::
option(cstring_view name) const
{
    assert(m_value);

    opt<std::string_view> result;
    char* value {};
    size_t len {};

    auto const rc = ::mnt_optstr_get_option(m_value, name, &value, &len);
    if (rc == 0) {
        result.emplace(value, len);
    }
    else if (rc == 1) {
        // nop
    }
    else if (rc < 0) {
        throw error {
            "mnt_optstr_get_option()",
            make_error_code(-rc), {
                { "name", name },
            }
        };
    }
    else {
        assert(false);
    }

    return result;
}

cstring optstr_view::
options(struct ::libmnt_optmap const* const map, int const ignore) const
{
    assert(m_value);

    char* subset {};
    auto const rc = ::mnt_optstr_get_options(m_value, &subset, map, ignore);
    if (rc < 0) {
        throw error {
            "mnt_optstr_get_options()",
            make_error_code(-rc), {
                { "ignore", ignore },
            }
        };
    }

    return cstring { subset };
}

unsigned long optstr_view::
flags(struct ::libmnt_optmap const* const map) const
{
    assert(m_value);

    unsigned long result {};

    auto const rc = ::mnt_optstr_get_flags(m_value, &result, map);
    if (rc < 0) {
        throw error { "mnt_optstr_get_flags()", make_error_code(-rc) };
    }

    return result;
}

split_result optstr_view::
split(bool const ignore_user/*= false*/, bool const ignore_vfs/*= false*/) const
{
    assert(m_value);

    split_result result {};
    char* user {};
    char* vfs {};
    char* fs {};

    auto const rc = ::mnt_split_optstr(
                            m_value, &user, &vfs, &fs, ignore_user, ignore_vfs);
    if (rc < 0) {
        throw error {
            "mnt_split_optstr()",
            make_error_code(-rc), {
                { "ignore_user", ignore_user },
                { "ignore_vfs", ignore_vfs },
            }
        };
    }

    if (user) {
        result.user.emplace(user);
    }
    if (vfs) {
        result.vfs.emplace(vfs);
    }
    if (fs) {
        result.fs.emplace(fs);
    }

    return result;
}

/*
 * class optstr_iterator
 */
optstr_iterator::
optstr_iterator(char* const optstr)
    : m_optstr { optstr }
{
    increment();
}

option const& optstr_iterator::
dereference() const noexcept
{
    return m_value;
}

void optstr_iterator::
increment()
{
    assert(m_optstr);

    char* name {};
    size_t namesz {};
    char* value {};
    size_t valuesz {};

    auto const rc = ::mnt_optstr_next_option(
                                   &m_optstr, &name, &namesz, &value, &valuesz);
    if (rc == 0) {
        m_value.name = { name, namesz };
        m_value.value = { value, valuesz };
    }
    if (rc == 1) {
        m_end = true;
    }
    if (rc < 0) {
        throw error {
            "mnt_optstr_next_option()",
            make_error_code(-rc), {
                { "optstr", m_optstr },
            }
        };
    }
    else {
        assert(false);
    }
}

bool optstr_iterator::
equal(std::default_sentinel_t) const noexcept
{
    return m_end;
}

} // namespace stream9::libmount
