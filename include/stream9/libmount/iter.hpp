#ifndef STREAM9_LIBMOUNT_ITER_HPP
#define STREAM9_LIBMOUNT_ITER_HPP

#include <libmount.h>

namespace stream9::libmount {

class iter
{
public:
    // essential
    iter(int direction);

    ~iter() noexcept;

    iter(iter const&) = delete;
    iter& operator=(iter const&) = delete;

    iter(iter&&) noexcept;
    iter& operator=(iter&&) noexcept;

    friend void swap(iter&, iter&) noexcept;

    // query
    int direction() const noexcept;

    // modifier
    void reset(int direction) noexcept;

    // converter
    operator ::libmnt_iter* () const noexcept { return m_handle; }

private:
    ::libmnt_iter* m_handle;
};

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_ITER_HPP
