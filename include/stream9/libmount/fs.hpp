#ifndef STREAM9_LIBMOUNT_FS_HPP
#define STREAM9_LIBMOUNT_FS_HPP

#include "cstring.hpp"
#include "cstring_view.hpp"
#include "json.hpp"
#include "opt.hpp"

#include <iosfwd>
#include <string_view>

#include <libmount.h>
#include <stdio.h>

namespace stream9::libmount {

class cache;
class table;

class fs
{
public:
    // essential
    fs();
    explicit fs(::libmnt_fs&) noexcept;

    ~fs() noexcept;

    fs(fs const&) noexcept;
    fs& operator=(fs const&) noexcept;

    fs(fs&&) noexcept;
    fs& operator=(fs&&) noexcept;

    friend void swap(fs&, fs&) noexcept;

    // query
    opt<class table> table() const noexcept;
    void* userdata() const noexcept;

    opt<cstring_view> source() const noexcept;
    opt<cstring_view> srcpath() const noexcept;

    struct tag_t {
        cstring_view name;
        cstring_view value;
    };
    opt<tag_t> tag() const noexcept;

    opt<cstring_view> target() const noexcept;
    opt<cstring_view> fstype() const noexcept;

    opt<cstring_view> options() const noexcept;
    opt<cstring_view> fs_options() const noexcept;
    opt<cstring_view> vfs_options() const noexcept;
    opt<cstring_view> user_options() const noexcept;
    opt<cstring_view> optional_fields() const noexcept;

    opt<std::string_view> option(cstring_view name) const;

    cstring strdup_options() const;
    cstring vfs_options_all() const;

    unsigned long propergation() const noexcept;

    opt<cstring_view> attributes() const noexcept;
    opt<std::string_view> attribute(cstring_view name) const;

    int freq() const noexcept;
    int passno() const noexcept;

    opt<cstring_view> root() const noexcept;
    opt<cstring_view> bindsrc() const noexcept;

    int id() const noexcept;
    int parent_id() const noexcept;

    ::dev_t devno() const noexcept;
    ::pid_t tid() const noexcept;

    opt<cstring_view> swaptype() const noexcept;
    ::off_t size() const noexcept;
    ::off_t usedsize() const noexcept;
    int priority() const noexcept;

    opt<cstring_view> comment() const noexcept;

    bool match_fstype(cstring_view types) const noexcept;
    bool match_options(cstring_view options) const noexcept;

    bool is_kernel() const noexcept;
    bool is_swaparea() const noexcept;
    bool is_netfs() const noexcept;
    bool is_pseudofs() const noexcept;
    //bool is_regularfs() const noexcept; //TODO absent on v2.37.2

    // modifier
    void set_userdata(void* data) noexcept;
    void set_source(cstring_view source);
    void set_target(cstring_view target);
    void set_fstype(cstring_view fstype);

    void set_options(cstring_view optstr);
    void append_options(cstring_view optstr);
    void prepend_options(cstring_view optstr);

    void set_attributes(cstring_view optstr);
    void append_attributes(cstring_view optstr);
    void prepend_attributes(cstring_view optstr);

    void set_freq(int freq) noexcept;
    void set_passno(int passno) noexcept;

    void set_root(cstring_view path);
    void set_bindsrc(cstring_view src);

    void set_priority(int prio) noexcept;

    void set_comment(cstring_view comm);
    void append_comment(cstring_view comm);

    // comparison
    bool streq_srcpath(cstring_view path) const noexcept;
    bool streq_target(cstring_view path) const noexcept;

    bool match_source(cstring_view path) const noexcept;
    bool match_source(cstring_view path, cache&) const noexcept;
    bool match_target(cstring_view target) const noexcept;
    bool match_target(cstring_view target, cache&) const noexcept;

    // command
    void reset();

    fs copy();
    void copy_to(fs& dest);

    void print_debug(FILE* file = stdout) const;

    // conversion
    operator libmnt_fs* () const noexcept { return m_handle; }

    friend std::ostream& operator<<(std::ostream&, fs const&);

private:
    ::libmnt_fs* m_handle;
};

void tag_invoke(json::value_from_tag, json::value&, fs const&);

struct ::mntent* to_mntent(fs&, struct ::mntent* mnt = {});

using ::mnt_free_mntent;

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_FS_HPP
