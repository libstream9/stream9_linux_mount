#ifndef STREAM9_LIBMOUNT_OPTSTR_HPP
#define STREAM9_LIBMOUNT_OPTSTR_HPP

#include "cstring.hpp"
#include "cstring_view.hpp"
#include "opt.hpp"
#include "namespace.hpp"

#include <iterator>
#include <string_view>

#include <libmount.h>

#include <stream9/iterators.hpp>

namespace stream9::libmount {

class optstr_iterator;

struct split_result {
    opt<cstring> user;
    opt<cstring> vfs;
    opt<cstring> fs;
};

class optstr
{
public:
    using iterator = optstr_iterator;
    using sentinel = std::default_sentinel_t;

public:
    // essential
    optstr();

    optstr(std::string_view);

    ~optstr() noexcept;

    optstr(optstr const&);
    optstr& operator=(optstr const&);

    optstr(optstr&&) noexcept;
    optstr& operator=(optstr&&) noexcept;

    friend void swap(optstr&, optstr&) noexcept;

    // accessor
    iterator begin() const;
    sentinel end() const noexcept;

    // query
    opt<std::string_view> option(cstring_view name) const;

    cstring options(struct ::libmnt_optmap const*, int ignore) const;

    unsigned long flags(struct ::libmnt_optmap const*) const;

    split_result
        split(bool ignore_user = false, bool ignore_vfs = false) const;

    // modifier
    void set_option(cstring_view name, cstring_view value);

    void append_option(cstring_view name, cstring_view value);
    void prepend_option(cstring_view name, cstring_view value);

    bool remove_option(cstring_view name);

    bool deduplicate_option(cstring_view name);

    void apply_flags(unsigned long flags,
                     struct ::libmnt_optmap const*);

    // conversion
    operator char const* () const noexcept { return m_value; }

private:
    char* m_value;
};

class optstr_view
{
public:
    using iterator = optstr_iterator;
    using sentinel = std::default_sentinel_t;

public:
    // essential
    optstr_view() = default; // partially-formed

    optstr_view(cstring_view) noexcept;

    // accessor
    iterator begin() const;
    sentinel end() const noexcept;

    // query
    opt<std::string_view> option(cstring_view name) const;

    cstring options(struct ::libmnt_optmap const*, int ignore) const;

    unsigned long flags(struct ::libmnt_optmap const*) const;

    split_result
        split(bool ignore_user = false, bool ignore_vfs = false) const;

    // conversion
    operator char const* () const noexcept { return m_value; }

private:
    char const* m_value;
};

struct option
{
    std::string_view name;
    std::string_view value;
};

class optstr_iterator : public itr::iterator_facade<optstr_iterator,
                                        std::input_iterator_tag,
                                        option const& >
{
public:
    optstr_iterator() = default; // partially-formed

    optstr_iterator(char* optstr);

private:
    friend itr::iterator_core_access;

    option const& dereference() const noexcept;

    void increment();

    bool equal(std::default_sentinel_t) const noexcept;

private:
    char* m_optstr {};
    option m_value;
    bool m_end = false;
};

} // namespace stream9::libmount

#if 0
/* optstr.c */
extern int mnt_optstr_append_option(char **optstr, const char *name,
				const char *value);
extern int mnt_optstr_prepend_option(char **optstr, const char *name,
				const char *value);

extern int mnt_optstr_set_option(char **optstr, const char *name,
				const char *value);
extern int mnt_optstr_remove_option(char **optstr, const char *name);
extern int mnt_optstr_deduplicate_option(char **optstr, const char *name);

extern int mnt_optstr_apply_flags(char **optstr, unsigned long flags,
                                const struct libmnt_optmap *map);
#endif
#endif // STREAM9_LIBMOUNT_OPTSTR_HPP
