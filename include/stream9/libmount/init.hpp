#ifndef STREAM9_LIBMOUNT_INIT_HPP
#define STREAM9_LIBMOUNT_INIT_HPP

namespace stream9::libmount {

void init_debug(int mask) noexcept;

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_INIT_HPP
