#ifndef STREAM9_LIBMOUNT_UPDATE_HPP
#define STREAM9_LIBMOUNT_UPDATE_HPP

#include "cstring_view.hpp"
#include "opt.hpp"
#include "lock.hpp"

#include <libmount.h>

namespace stream9::libmount {

class fs;
class lock;

class update
{
public:
    // essential
    update();

    ~update() noexcept;

    update(update const&) = delete;
    update& operator=(update const&) = delete;

    update(update&&) noexcept;
    update& operator=(update&&) noexcept;

    friend void swap(update&, update&) noexcept;

    // query
    bool is_ready() const noexcept;

    opt<cstring_view> filename() const noexcept;
    opt<class fs> fs() const noexcept;
    unsigned long mflags() const noexcept;

    // modifier
    void set_mount_fs(unsigned long mountflags, class fs const&);
    void set_umount_target(unsigned long mountflags, cstring_view target);

    void force_rdonly(bool rdonly);

    // command
    void update_table(opt<lock> = {});

    // conversion
    operator ::libmnt_update* () const noexcept { return m_handle; }

private:
    libmnt_update* m_handle;
};

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_UPDATE_HPP
