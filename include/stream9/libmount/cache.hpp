#ifndef STREAM9_LIBMOUNT_CACHE_HPP
#define STREAM9_LIBMOUNT_CACHE_HPP

#include "cstring.hpp"
#include "cstring_view.hpp"
#include "opt.hpp"

#include <libmount.h>

namespace stream9::libmount {

class table;

class cache
{
public:
    // essential
    cache();
    explicit cache(::libmnt_cache&) noexcept;

    ~cache() noexcept;

    cache(cache const&) noexcept;
    cache& operator=(cache const&) noexcept;

    cache(cache&&) noexcept;
    cache& operator=(cache&&) noexcept;

    friend void swap(cache&, cache&) noexcept;

    // query
    bool device_has_tag(cstring_view devname,
                        cstring_view token,
                        cstring_view value) const noexcept;

    cstring_view find_tag_value(cstring_view devname,
                                cstring_view token) const noexcept;

    // modifier
    void set_targets(table& mtab) noexcept;
    bool read_tags(cstring_view devname);

    // conversion
    operator ::libmnt_cache* () const noexcept { return m_handle; }

private:
    ::libmnt_cache* m_handle;
};

struct cached_get_fstype_result {
    cstring_view value;
    bool ambivalent;
};
opt<cached_get_fstype_result>
get_fstype(cstring_view devname, cache&) noexcept;

struct get_fstype_result {
    cstring value;
    bool ambivalent;
};
opt<get_fstype_result>
get_fstype(cstring_view devname) noexcept;

opt<cstring_view> resolve_path(cstring_view path, cache&) noexcept;
opt<cstring>      resolve_path(cstring_view path) noexcept;

opt<cstring_view> resolve_target(cstring_view path, cache&) noexcept;
opt<cstring>      resolve_target(cstring_view path) noexcept;

opt<cstring_view> resolve_tag(cstring_view token, cstring_view value, cache&) noexcept;
opt<cstring>      resolve_tag(cstring_view token, cstring_view value) noexcept;

opt<cstring_view> resolve_spec(cstring_view spec, cache&) noexcept;
opt<cstring>      resolve_spec(cstring_view spec) noexcept;

opt<cstring> pretty_path(cstring_view path, cache&) noexcept;
opt<cstring> pretty_path(cstring_view path) noexcept;

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_CACHE_HPP
