#ifndef STREAM9_LIBMOUNT_CSTRING_HPP
#define STREAM9_LIBMOUNT_CSTRING_HPP

#include <stream9/c.hpp>

namespace stream9::libmount {

using cstring = stream9::c::string;

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_CSTRING_HPP
